import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { EmpresasComponent } from './empresa/empresas.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormEmpresaComponent } from './empresa/form-empresa.component';
import { FormsModule } from '@angular/forms';
import { EstudiantesComponent } from './estudiante/estudiantes.component';

const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: 'empresas', component: EmpresasComponent },
  { path: 'empresas/form', component: FormEmpresaComponent },
  { path: 'empresas/form/:nit', component: FormEmpresaComponent },
  { path: 'estudiantes', component: EstudiantesComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    EmpresasComponent,
    FormEmpresaComponent,
    EstudiantesComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

export class Empresa {
  nit: number;

  nombre: string;

  pais: string;

  departamento: string;

  ciudad: string;

  codigoPostal: string;

  correoElectronico: string;

  telefono: string;

  paginaWeb: string;

  cargado: boolean;

  constructor() {
    this.nit = null;
    this.nombre = '';
    this.pais = 'Afghanistan';
    this.departamento = '';
    this.ciudad = '';
    this.codigoPostal = '';
    this.telefono = '';
    this.correoElectronico = '';
    this.paginaWeb = '';
    this.cargado = false;
  }
}

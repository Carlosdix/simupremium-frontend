import { Component, OnInit } from '@angular/core';
import { PaisesService } from './paises.service';
import { Pais } from './pais';
import { Empresa } from './empresa';
import { EmpresasService } from './empresas.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form-empresa',
  templateUrl: './form-empresa.component.html',
  styleUrls: ['./form-empresas.component.css'],
})
export class FormEmpresaComponent implements OnInit {
  empresa: Empresa = new Empresa();
  paises: Pais[] = [];

  constructor(
    private paiseApi: PaisesService,
    private empresaService: EmpresasService,
    private router: Router,
    private activatedRouted: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.cargarEmpresa();
    this.paiseApi.getAll().subscribe((paises) => (this.paises = paises));
  }

  cargarEmpresa(): void {
    this.activatedRouted.params.subscribe((params) => {
      const nit = params.nit;
      if (nit) {
        this.empresaService
          .getEmpresa(nit)
          .subscribe((empresa) => (this.empresa = empresa));
      }
    });
  }

  crearEmpresa(): void {
    this.empresaService.crearEmpresa(this.empresa).subscribe((response) => {
      this.router.navigate(['/empresas']);
      swal.fire(
        'Registro realizado',
        `Empresa ${this.empresa.nombre} los cambios se han registrado con exito!`,
        'success'
      );
    });
  }

  actualizar(): void {
    this.empresaService.actualizar(this.empresa).subscribe((empresa) => {
      this.router.navigate(['/empresas']);
      swal.fire(
        `Empresa actualizada`,
        `Empresa ${
          this.empresa.nombre + ' - ' + this.empresa.nit
        } actualizado con exito`,
        'info'
      );
    });
  }
}

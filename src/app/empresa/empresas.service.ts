import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Empresa } from './empresa';
import { map } from 'rxjs/operators';
// import { EMPRESAS } from './empresas.json';

@Injectable({
  providedIn: 'root',
})
export class EmpresasService {
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  /*
  http_options = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      Authorization: 'authkey',
      userid: '1',
    }),
  };
  */

  private urlEndpoint = 'http://localhost:9000/empresas/';

  constructor(private http: HttpClient) {}

  getEmpresa(nit: number): Observable<Empresa> {
    return this.http.get<Empresa>(`${this.urlEndpoint}${nit}`);
  }

  listarEmpresas(): Observable<Empresa[]> {
    return this.http
      .get(`${this.urlEndpoint}all`)
      .pipe(map((response) => response as Empresa[]));
    // return this.http.get<Empresa[]>(`${this.urlEndpoint}all`)
    // return of(EMPRESAS)
  }

  crearEmpresa(pEmpresa: Empresa): Observable<Empresa> {
    return this.http.post<Empresa>(`${this.urlEndpoint}all`, pEmpresa, {
      headers: this.httpHeaders,
    });
  }

  actualizar(pEmpresa: Empresa): Observable<Empresa> {
    return this.http.put<Empresa>(
      `${this.urlEndpoint}${pEmpresa.nit}`,
      pEmpresa,
      { headers: this.httpHeaders }
    );
  }

  eliminar(nit: number): Observable<Empresa> {
    return this.http.delete<Empresa>(`${this.urlEndpoint}${nit}`, {
      headers: this.httpHeaders,
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { Empresa } from './empresa';
// import { EMPRESAS } from './empresas.json';
import { EmpresasService } from './empresas.service';
import { PaisesService } from './paises.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-empresas',
  templateUrl: './empresas.component.html',
  styleUrls: ['./empresas.component.css'],
})
export class EmpresasComponent implements OnInit {
  empresas: Empresa[] = [];

  constructor(private service: EmpresasService) {}

  ngOnInit(): void {
    this.getEmpresas();
  }

  getEmpresas(): void {
    this.service
      .listarEmpresas()
      .subscribe((empresas) => (this.empresas = empresas));
  }

  elmininar(pEmpresa: Empresa): void {
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: '¿Estas segurode eliminar a esta empresa?',
        text: `Estas apunto de eliminar a ${
          pEmpresa.nombre + ' - ' + pEmpresa.nit
        }`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, borrar empresa!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.service.eliminar(pEmpresa.nit).subscribe((response) => {
            this.empresas = this.empresas.filter(
              (empresa) => empresa !== pEmpresa
            );
            swalWithBootstrapButtons.fire(
              'Eliminado!',
              'La empresa ha sido eliminada con exito',
              'success'
            );
          });
        }
      });
  }
}

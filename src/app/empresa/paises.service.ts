import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Pais} from './pais';

@Injectable({
  providedIn: 'root',
})
export class PaisesService {
  private urlEnpoint: string = 'https://restcountries.eu/rest/v2/';
  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get<Pais[]>(`${this.urlEnpoint}all`)
  }
}

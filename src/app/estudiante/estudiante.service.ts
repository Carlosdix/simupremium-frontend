import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Estudiante } from './estudiante';
import { ESTUDIANTES } from './estudiantes.json';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class EstudianteService {
  urlEndPoint = 'http://localhost:9000/estudiantes/';
  constructor(private http: HttpClient) {}

  listadoEstudiantes(): Observable<Estudiante[]> {
    return this.http.get<Estudiante[]>(`${this.urlEndPoint}all`);
  }
}

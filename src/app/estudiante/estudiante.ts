export class Estudiante {
  documento: number;

  nombres: string;

  apellidos: string;

  correo: string;

  celular: string;

  direccion: string;

  constructor() {
    this.documento = 0;
    this.nombres = '';
    this.apellidos = '';
    this.correo = '';
    this.celular = '';
    this.direccion = '';
  }
}
